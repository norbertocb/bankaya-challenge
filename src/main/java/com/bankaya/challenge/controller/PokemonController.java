package com.bankaya.challenge.controller;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bankaya.challenge.model.dto.AbilitiesOutDto;
import com.bankaya.challenge.model.dto.BaseExperienceOutDto;
import com.bankaya.challenge.model.entity.RequestLog;
import com.bankaya.challenge.service.PokemonAbilitiesService;
import com.bankaya.challenge.service.PokemonBaseExperienceService;
import com.bankaya.challenge.service.RequestLogService;

/**
 * Contoller for pokemon info
 * @author Norberto Camacho 
 *
 */
@RestController
@RequestMapping("/api")
public class PokemonController {
	
	/**
	 * Class logger
	 */
	private Logger LOGGER = LoggerFactory.getLogger(PokemonController.class);

	/**
	 * Service for pokemon abilities
	 */
	@Autowired
	private PokemonAbilitiesService pokemonService;
	
	/**
	 * Service for base experience
	 */
	@Autowired
	private PokemonBaseExperienceService pokemonBaseExperienceService;
	
	/**
	 * Service for request log in database
	 */
	@Autowired
	private RequestLogService requestLogService;
	
	/**
	 * Get abilities
	 * @param pokemon name
	 * @return response entity
	 */
	@GetMapping(path = "/abilities/{pokemon}")
	public ResponseEntity<AbilitiesOutDto> ability(@PathVariable(value = "pokemon") 
		@NotBlank @Size(min=2, max = 20) String pokemon){
		
		LOGGER.info("Get Service for abilities: {}", pokemon);
		
		return pokemonService.getAbilities(pokemon.toLowerCase());
	}
	
	/**
	 * Get base_experience pokemon
	 * @param pokemon pokemon name
	 * @return response with base experience
	 */
	@GetMapping(path="/base_experience/{pokemon}")
	public ResponseEntity<BaseExperienceOutDto> baseExperience(@PathVariable(value = "pokemon") 
		@NotBlank @Size(min=2, max = 20) String pokemon){
		LOGGER.info("Get Service for base experience: {}", pokemon);
		
		return pokemonBaseExperienceService.getBaseExperience(pokemon.toLowerCase());
	}

	/**
	 * Get request log from Database
	 * 
	 * @return Records from database request log
	 */
	@GetMapping(path="/requestlog")
	public List<RequestLog> requestLog(){
		LOGGER.info("Get Service for Request Logs in database");
		
		return requestLogService.findAll();
	}
	
}
