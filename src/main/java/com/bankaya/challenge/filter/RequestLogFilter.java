package com.bankaya.challenge.filter;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bankaya.challenge.service.RequestLogService;

/**
 * Filter for request listener
 * @author Norbero Camacho
 *
 */
@Configuration
public class RequestLogFilter { 

	/**
	 * Logger
	 */
	private Logger LOGGER = LoggerFactory.getLogger(RequestLogFilter.class);
			
	/**
	 * Service for database
	 */
	@Autowired
	private RequestLogService requestLogService;
	
	/**
	 * Filter for request log
	 * @return
	 */
    @Bean
    public FilterRegistrationBean<Filter> requestFilter() {
        FilterRegistrationBean<Filter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setName("requestFilter");
        registrationBean.setOrder(1);
        registrationBean.setFilter((request, response, chain) -> {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            LOGGER.info("request from {}", httpServletRequest.getRequestURI());
            requestLogService.saveRequest(httpServletRequest.getRemoteAddr(), httpServletRequest.getMethod() + ":" + httpServletRequest.getRequestURI());
            chain.doFilter(request,response);
        });
        registrationBean.addUrlPatterns("/api/*");
        return registrationBean;
    }
}
