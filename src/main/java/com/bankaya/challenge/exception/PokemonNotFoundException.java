package com.bankaya.challenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Customization for exception
 * 
 * @author Norberto Camacho
 *
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "not found")
public class PokemonNotFoundException extends RuntimeException {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	
}

