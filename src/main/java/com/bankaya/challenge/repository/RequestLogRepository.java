package com.bankaya.challenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bankaya.challenge.model.entity.RequestLog;

/**
 * Repository for RequestLog
 * 
 * @author Norberto Camacho
 *
 */
@Repository
public interface RequestLogRepository extends JpaRepository<RequestLog, Long>{

}
