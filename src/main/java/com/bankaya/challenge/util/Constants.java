package com.bankaya.challenge.util;

/**
 * 
 * Constants for project
 * 
 * @author Norberto Camacho
 *
 */
public class Constants {
	/**
	 * Private constructor for avoid new instances
	 */
	private Constants() {};
	
	/**
	 * Error label
	 */
	public static final String ERROR = "ERROR";
	
	/**
	 * OK label
	 */
	public static final String OK = "OK";
	
	/**
	 * abilities
	 */
	public static final String ABILITIES = "abilities";
	
	/**
	 * base_experience
	 */
	public static final String BASE_EXPERIENCE = "base_experience";
	
}
