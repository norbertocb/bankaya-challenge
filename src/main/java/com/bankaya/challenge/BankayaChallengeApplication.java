package com.bankaya.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * App fpr pokemons info and skills
 * @author Norberto Camacho
 *
 */
@SpringBootApplication
public class BankayaChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankayaChallengeApplication.class, args);
	}

}
