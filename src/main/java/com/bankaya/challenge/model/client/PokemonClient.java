package com.bankaya.challenge.model.client;

import com.fasterxml.jackson.databind.JsonNode;

import feign.Param;
import feign.RequestLine;

/**
 * Pokemon Feing client
 * @author Norberto Camacho
 *
 */
public interface PokemonClient {

	/**
	 * Get request for Pokemon API
	 * @param pokemon name
	 * @return Nodes
	 */
	@RequestLine("GET /{pokemon}")
	JsonNode findByPokemon(@Param(value = "pokemon") String pokemon);
}
