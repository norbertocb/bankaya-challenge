package com.bankaya.challenge.model.dto;

import java.util.List;

import lombok.Data;

@Data
public class OutDtoBase {
	
		/**
		 * Response status
		 */
		private String status;
		
		/**
		 * Error list
		 */
		private List<String> errors;
}
