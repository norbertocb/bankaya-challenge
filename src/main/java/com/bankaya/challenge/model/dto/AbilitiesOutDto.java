package com.bankaya.challenge.model.dto;

import java.util.List;

import lombok.Data;

/**
 * Service bean response
 * @author Norberto Camacho
 *
 */
@Data
public class AbilitiesOutDto extends OutDtoBase{
	
	/**
	 * abilities list
	 */
	private List<Abilities> abilities;
	
	
	/**
	 * Constructor all params
	 * @param status status
	 * @param errros errors
	 * @param abilities  abilities pokemon
	 */
	public AbilitiesOutDto(String status, List<String> errors, List<Abilities> abilities) {
		this.setStatus(status);
		this.setErrors(errors);
		this.abilities = abilities;
	}
	
	/**
	 * Constructor status and errors
	 * 
	 * @param status status 
	 * @param errors errors
	 */
	public AbilitiesOutDto(String status, List<String> errors) {
		this.setStatus(status);
		this.setErrors(errors);
	}
	
	/**
	 * Constructor status
	 * @param status status
	 */
	public AbilitiesOutDto(String status) {
		this.setStatus(status);
	}

}