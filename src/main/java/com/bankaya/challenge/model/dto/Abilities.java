package com.bankaya.challenge.model.dto;

import java.util.List;

import lombok.Data;

/**
 * Object for abilities
 * @author Norberto Camacho
 *
 */
@Data
public class Abilities {

	/**
	 * abilities
	 */
	private List<Ability> abilities;
	
	/**
	 * isHidden
	 */
	private  boolean is_hidden;
	
	/**
	 * slot
	 */
    private long slot;
}
