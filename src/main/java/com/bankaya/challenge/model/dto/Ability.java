package com.bankaya.challenge.model.dto;

import lombok.Data;

/**
 * Ability pojo
 * @author Norberto Camacho
 *
 */
@Data
public class Ability {

	/**
	 * Name
	 */
	private String name;
	
	/**
	 * Url
	 */
	private String url;
}
