package com.bankaya.challenge.model.dto;

import java.util.List;

import lombok.Data;

@Data
public class BaseExperienceOutDto extends OutDtoBase{

	private String  baseExperience;
	
	public BaseExperienceOutDto(String status, List<String> errors, String baseExperience){
		this.setStatus(status);
		this.setErrors(errors);
		this.baseExperience = baseExperience;
	}
}
