package com.bankaya.challenge.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Entity for request log
 * 
 * @author Norberto Camacho
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(schema = "BANKAYA")
public class RequestLog {

	/**
	 * Id for table
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	/**
	 * Ip origin
	 */
	@Column(nullable = false, length = 20)
	private String ipOrigin;

	/**
	 * Request Date
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date requestDatetime;
	
	/**
	 * Method called
	 */
	@Column(nullable = false, length = 200)
	private String method; 
	
}
