package com.bankaya.challenge.service;

import com.bankaya.challenge.exception.PokemonNotFoundException;
import com.fasterxml.jackson.databind.JsonNode;

public interface PokemonApiService {
	
	public JsonNode consumePokemonApi(String pokemonName) throws PokemonNotFoundException;
}
