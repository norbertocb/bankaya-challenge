package com.bankaya.challenge.service;

import org.springframework.http.ResponseEntity;

import com.bankaya.challenge.model.dto.BaseExperienceOutDto;

public interface PokemonBaseExperienceService {

	public ResponseEntity<BaseExperienceOutDto> getBaseExperience(String pokemonName);
}
