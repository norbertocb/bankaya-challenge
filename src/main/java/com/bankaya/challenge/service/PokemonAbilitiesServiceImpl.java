package com.bankaya.challenge.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bankaya.challenge.exception.PokemonNotFoundException;
import com.bankaya.challenge.model.dto.Abilities;
import com.bankaya.challenge.model.dto.AbilitiesOutDto;
import com.bankaya.challenge.util.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Service class for Pokemon
 * 
 * @author Norberto Camacho
 *
 */
@Service
public class PokemonAbilitiesServiceImpl implements PokemonAbilitiesService{

	/**
	 * Logger
	 */
	Logger LOGGER = LoggerFactory.getLogger(PokemonAbilitiesServiceImpl.class);
	
	/**
	 * Pokemon API
	 */
	@Autowired
	private PokemonApiService pokemonApiService;
	
	/**
	 * {@inheritDoc}
	 */
	public ResponseEntity<AbilitiesOutDto> getAbilities(String pokemonName){
		
		LOGGER.info("Get Pokemmon Abilities: {}" , pokemonName);
		
		AbilitiesOutDto abilitiesOutDto; 
		ObjectMapper mapper = new ObjectMapper();
		List<Abilities> abilities = new ArrayList<Abilities>();
		
		try {
			JsonNode object = pokemonApiService.consumePokemonApi(pokemonName);	
			abilities = mapper.treeToValue(object.get(Constants.ABILITIES), List.class);
			
		} catch (JsonProcessingException | IllegalArgumentException e) {
			LOGGER.error(e.getMessage());
			abilitiesOutDto = new AbilitiesOutDto(Constants.ERROR, new ArrayList<>(), abilities);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (PokemonNotFoundException e) {
			LOGGER.error(e.getMessage());
			throw new PokemonNotFoundException();
		}
		
		abilitiesOutDto = new AbilitiesOutDto(Constants.OK, new ArrayList<>(), abilities );

		LOGGER.info("Abilities for {} : {}", pokemonName, abilities.size());
		
		return new ResponseEntity<>(abilitiesOutDto, HttpStatus.OK);
	}
	
}