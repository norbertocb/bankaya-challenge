package com.bankaya.challenge.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bankaya.challenge.exception.PokemonNotFoundException;
import com.bankaya.challenge.model.client.PokemonClient;
import com.fasterxml.jackson.databind.JsonNode;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;

/**
 * Service for consume API Pokemon
 * @author norber
 *
 */
@Service
public class PokemonApiServiceImpl implements PokemonApiService{

	/**
	 * Logger
	 */
	Logger LOGGER = LoggerFactory.getLogger(PokemonApiServiceImpl.class);
	
	/**
	 * Enpoint for Pokemon Api
	 */
	@Value("${pokemon.api.endpoint}")
	private String pokemonApiEndpoint;
	
	/**
	 * Consume Pokemon API from web 
	 * 
	 * @param pokemonName name
	 * @return Nodes nodes for service
	 * @throws PokemonNotFoundException exception not found
	 */
	public JsonNode consumePokemonApi(String pokemonName) throws PokemonNotFoundException {
		LOGGER.info("Consumes services from Pokemon Api Online {}", pokemonName);
		PokemonClient pockemonResult = Feign.builder()
				.decode404()
				.client(new OkHttpClient())
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.target(PokemonClient.class, pokemonApiEndpoint);
		
		JsonNode nodeResult = pockemonResult.findByPokemon(pokemonName); 
		
		if( nodeResult == null)
			throw new PokemonNotFoundException();
		
		return pockemonResult.findByPokemon(pokemonName); 
	}
}
