package com.bankaya.challenge.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bankaya.challenge.exception.PokemonNotFoundException;
import com.bankaya.challenge.model.dto.BaseExperienceOutDto;
import com.bankaya.challenge.util.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class PokemonBaseExperienceServiceImpl implements PokemonBaseExperienceService{
	/**
	 * Logger
	 */
	Logger LOGGER = LoggerFactory.getLogger(PokemonBaseExperienceServiceImpl.class);
	
	/**
	 * Pokemon API
	 */
	@Autowired
	private PokemonApiService pokemonApiService;
	
	/**
	 * {@inheritDoc}
	 */
	public ResponseEntity<BaseExperienceOutDto> getBaseExperience(String pokemonName){
		
		LOGGER.info("Get Pokemmon BaseExperience: {}" , pokemonName);
		
		BaseExperienceOutDto baseExperienceOutDto; 
		ObjectMapper mapper = new ObjectMapper();
		String baseExperience = new String();
		
		try {
			JsonNode object = pokemonApiService.consumePokemonApi(pokemonName);	
			baseExperience = mapper.treeToValue(object.get(Constants.BASE_EXPERIENCE), String.class);
			
		} catch (JsonProcessingException | IllegalArgumentException e) {
			LOGGER.error(e.getMessage());
			baseExperienceOutDto = new BaseExperienceOutDto(Constants.ERROR, new ArrayList<>(), baseExperience);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (PokemonNotFoundException e) {
			LOGGER.error(e.getMessage());
			throw new PokemonNotFoundException();
		}
		
		baseExperienceOutDto = new BaseExperienceOutDto(Constants.OK, new ArrayList<>(), baseExperience );

		LOGGER.info("Pokemmon BaseExperience {} : {}", pokemonName, baseExperience);
		
		return new ResponseEntity<>(baseExperienceOutDto, HttpStatus.OK);
	}
}
