package com.bankaya.challenge.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankaya.challenge.model.entity.RequestLog;
import com.bankaya.challenge.repository.RequestLogRepository;

/**
 * Service for request log save on BD
 * @author Norberto Camaacho
 *
 */
@Service
public class RequestLogServiceImpl implements RequestLogService{
	
	/**
	 * Repository for request log
	 */
	@Autowired
	private RequestLogRepository requestLogRepository;
	
	/**
	 * {@inheritDoc}
	 */
	public void saveRequest(String remoteIpAddress, String method) {
		
		requestLogRepository.save(new RequestLog(null, remoteIpAddress, new Date(), method));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<RequestLog> findAll(){
		return requestLogRepository.findAll();
	}
	

}
