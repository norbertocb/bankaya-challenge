package com.bankaya.challenge.service;

import java.util.List;

import com.bankaya.challenge.model.entity.RequestLog;

/**
 * Request log service
 * @author Norberto Camacho
 *
 */
public interface RequestLogService {

	/**
	 * Save request log
	 * @param remoteIpAddress remote ip
	 * @param method web method
	 */
	public void saveRequest(String remoteIpAddress, String method);
	
	/**
	 * Get request Log
	 * @return List request log
	 */
	public List<RequestLog> findAll();
}
