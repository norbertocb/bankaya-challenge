package com.bankaya.challenge.service;

import org.springframework.http.ResponseEntity;

import com.bankaya.challenge.model.dto.AbilitiesOutDto;
import com.bankaya.challenge.model.dto.BaseExperienceOutDto;

/**
 * Pokemon service
 * @author Norberto Camacho
 *
 */
public interface PokemonAbilitiesService {

	/**
	 * 
	 * @param name
	 */
	public ResponseEntity<AbilitiesOutDto> getAbilities(String pokemonName);
	
}
