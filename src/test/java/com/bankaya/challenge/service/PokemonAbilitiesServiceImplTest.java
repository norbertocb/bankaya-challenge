package com.bankaya.challenge.service;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.bankaya.challenge.exception.PokemonNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;

@SpringBootTest
public class PokemonAbilitiesServiceImplTest {

	/**
	 * Pokemon API
	 */
	@Autowired
	private PokemonAbilitiesServiceImpl pokemonAbilitiesServiceImpl;

	
	@DisplayName("Test PokemonAbilitiesServiceImplTest")
    @Test
    void getAbilitiesOK() {
		
		assertEquals(ResponseEntity.class ,pokemonAbilitiesServiceImpl.getAbilities("pikachu").getClass());
    }
	
	@DisplayName("Test PokemonAbilitiesServiceImplTest excpetion")
    @Test
    void getAbilitiesThrowsException() {
		
		Assertions.assertThrows(PokemonNotFoundException.class,()  -> {
			pokemonAbilitiesServiceImpl.getAbilities("pikacha");
		});
    }

}